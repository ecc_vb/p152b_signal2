﻿' Project:P152B_Signal2
' Auther:IE2A No.24, 村田直人
' Date: 2015年05月15日

Imports P152B_Signal2.My.Resources


Public Class Form1

    Dim SigImages(2) As Image                               '信号イメージを格納する配列
    Dim ButtonTexts() As String = {"スタート", "ストップ"}  'ボタンのテキスト

    Dim SigPictureBoxes(1) As PictureBox '左右のPictureboxの変数

    'インターバルの順番フラグ
    Dim SigSequences(,) As Integer = {
    {0, 0, 0, 1, 1, 2, 2, 2, 2, 2, 2, 2}, _
        {2, 2, 2, 2, 2, 2, 0, 0, 0, 1, 1, 2}
    }

    '起動直後処理
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        SigPictureBoxes(0) = LeftPictureBox     '「左」のPicturebox
        SigPictureBoxes(1) = RightPictureBox    '「右」のPicturebox

        '信号のイメージを配列に格納
        SigImages(0) = SignalG      '緑
        SigImages(1) = SignalY      '黄色
        SigImages(2) = SignalR      '赤

        LeftPictureBox.Image = SigImages(0)     '画面に青信号を表示
        StartButton.Text = ButtonTexts(0)       'テキストを「スタート」に設定()

        SigPictureBoxes(0).Image = SigImages(0) '「左」Pictureboxに「青」を表示
        SigPictureBoxes(1).Image = SigImages(2) '「右」Pictureboxに「赤」を表示

    End Sub

    'ボタンクリック処理
    Private Sub StartButton_Click(sender As Object, e As EventArgs) Handles StartButton.Click

        Static s As Integer '「スタート」と「ストップ」の切り替えフラグ

        'タイマーの「起動」,「停止」処理
        If s = 0 Then

            s = 1           'フラグ更新

        ElseIf s = 1 Then

            s = 0           'フラグ更新

        End If

        Timer1.Enabled = CType(s, Boolean)  'タイマー起動
        StartButton.Text = ButtonTexts(s)   'ボタンのテキストを設定

    End Sub

    'タイマー処理
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        Static n As Integer '信号の切り替えフラグ
        n = (n + 1) Mod 12  'フラグの更新

        For p As Integer = 0 To 1

            SigPictureBoxes(p).Image = SigImages(SigSequences(p, n)) '画像の変更

        Next

    End Sub
End Class
